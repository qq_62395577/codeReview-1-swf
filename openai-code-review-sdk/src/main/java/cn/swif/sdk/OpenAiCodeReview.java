package cn.swif.sdk;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @FileName openaiAiCodeReview
 * @Description
 * @Author swifnotswift
 * @date 2025-02-15
 **/
public class OpenAiCodeReview {
//    public static void main(String[] args) throws IOException, InterruptedException {
//
////        1.代码检出操作
//        ProcessBuilder processBuilder = new ProcessBuilder("git", "diff", "HEAD~1", "HEAD");
//        processBuilder.directory(new File("."));
//        Process process = processBuilder.start();
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
//        String line;
//        StringBuffer diffcode=new StringBuffer();
//        while ((line=bufferedReader.readLine())!=null){
//            diffcode.append(line);
//        }
//
//        int i = process.waitFor();
//        System.out.println("Exited Code:"+i);
//        System.out.println("代码评审"+diffcode.toString());
//
//
//    }
public static void main(String[] args) throws Exception {
    System.out.println("测试执行");

    // 1. 代码检出
    ProcessBuilder processBuilder = new ProcessBuilder("git", "diff", "HEAD~1", "HEAD");
    processBuilder.directory(new File("."));

    Process process = processBuilder.start();

    BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
    String line;

    StringBuilder diffCode = new StringBuilder();
    while ((line = reader.readLine()) != null) {
        diffCode.append(line);
    }

    int exitCode = process.waitFor();
    System.out.println("Exited with code:" + exitCode);

    System.out.println("评审代码：" + diffCode.toString());
}

}
